#include <math.h>

#include <iostream>

using namespace std;

// Function to solve tridiagonal matrix
float *Tridiagonal(float r, float d[], int n, float *x) {
    int i;
    float a = -r / 2, b = (1 + r), c = -r / 2;

    float alpha[10], beta[10];

    alpha[1] = b;
    beta[1] = d[1] / b;

    for (i = 2; i < n; i++) {
        alpha[i] = b - (a * c / alpha[i - 1]);
        beta[i] = (d[i] - a * beta[i - 1]) / alpha[i];
    }

    x[n] = beta[n];

    for (i = n - 1; i >= 1; i--) {
        x[i] = beta[i] - (c * x[i + 1]) / alpha[i];
    }
    return x;
}

// Initial value condition
float f(float x) {
    return (x / 3) * (16 - pow(x, 2));
}

int main() {
    int i, j, l, N, M;

    cout << "Enter number of intervals: ";
    cin >> N;

    float x[N];
    cout << "Enter number of timesteps: ";
    cin >> M;
    float u[N + 1][M + 1] = {0};
    float c2, h = 1, k = 0.1;

    cout << "Enter the value of c^2: ";
    cin >> c2;

    float r = c2 * k / pow(h, 2);

    // Setting boundary values
    for (int i = 0; i <= M; i++) {
        u[0][i] = 0;
        u[N][i] = 0;
    }

    // Setting initial conditions
    for (i = 1; i < N; i++) {
        u[i][0] = f(i * h);
    }

    float d[N] = {0};

    for (j = 0; j < M; j++) {
        for (i = 1; i < N; i++) {
            // Crank-Nicolson Formula
            d[i] = (r / 2) * u[i + 1][j] + (1 - r) * u[i][j] +
                   (r / 2) * u[i - 1][j];
        }
        float *y = Tridiagonal(r, d, N, x);

        for (l = 1; l < N; l++) {
            u[l][j + 1] = y[l];
        }
    }

    // Displaying values
    for (j = 0; j <= M; j++) {
        for (i = 0; i <= N; i++) {
            cout << u[i][j] << " ";
        }
        cout << "\n";
    }
}