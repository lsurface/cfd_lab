#README #

M.Sc. 2nd Year - MCC518

### What is this repository for? ###

Assignment 1 - Heat Conduction using explicit method

Assignment 2 - Heat Conduction using implicit and semi-implicit method

Assignment 3 - Laplace equation using Liebmann iteration

Assignment 3 - Question 2 Poisson Equation using SOR iteration

Assignment 4 - Wave Equation using Explicit method