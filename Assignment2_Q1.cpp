#include <math.h>

#include <iostream>

using namespace std;

float f(float x) {
    return 0;  // Add initial condition
}

int main() {
    int i, j, l, N, M;

    cout << "Enter number of intervals: ";
    cin >> N;

    cout << "Enter number of time levels: ";
    cin >> M;
    float u[N + 1][M + 1] = {0};
    float r, h = 1, k = 0.1, c2, e, temp;

    cout << "Enter the value of c^2: ";
    cin >> c2;

    r = c2 * k / pow(h, 2);

    // Assigning boundary conditions
    for (int i = 0; i <= M; i++) {
        u[0][i] = 0;
        u[N][i] = 0;
    }

    // Assigning initial conditions
    for (int i = 0; i <= N; i++) {
        u[i][0] = f(i * h);
    }

    // Initializing remaining node points value as zero
    for (j = 1; j <= M; j++) {
        for (i = 1; i < N; i++) {
            u[i][j] = 0;
        }
    }

    // Solving the node values iteratively
    for (j = 0; j < M; j++) {
        bool flag = true;
        while (flag == true) {
            flag = false;
            for (i = 1; i < N; i++) {
                temp = u[i][j + 1];
                u[i][j + 1] =
                    (r * u[i - 1][j + 1] + r * u[i + 1][j + 1] + u[i][j]) /
                    (1 + 2 * r);
                e = abs(temp - u[i][j + 1]);

                if (e > 0.001) {
                    flag = true;
                }
            }
        }
    }

    for (j = 0; j <= M; j++) {
        for (i = 0; i <= N; i++) {
            cout << u[i][j] << " ";
        }
        cout << "\n";
    }

    return 0;
}