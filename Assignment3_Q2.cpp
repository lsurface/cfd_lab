#include <math.h>

#include <iostream>

using namespace std;

float func(float x, float y) {
    return -4 * (pow(x, 2) + pow(y, 2));
}
int main() {
    int n, m;

    // Relaxation factor > 1
    float w = 1.5;

    // Run using n=3, m=3
    cout << "Enter value for x-intervals: ";
    cin >> n;

    cout << "Enter value for y-intervals: ";
    cin >> m;

    float u[n + 1][m + 1];

    for (int i = 0; i <= n; i++) {
        for (int j = 0; j <= m; j++) {
            u[i][j] = 0;
        }
    }
    float k_u[n + 1][m + 1];

    float h = (3 - 0) / n;

    float error = 0.005;
    bool flag = true;
    int k = 1;
    while (flag) {
        for (int i = 0; i <= n; i++) {
            for (int j = 0; j <= m; j++) {
                k_u[i][j] = u[i][j];
            }
        }

        // SOR Approximation
        for (int i = 1; i < n; i++) {
            for (int j = 1; j < m; j++) {
                u[i][j] =
                    (w / 4) * (k_u[i + 1][j] + u[i - 1][j] + k_u[i][j + 1] +
                               k_u[i][j - 1] - pow(h, 2) * func(i * h, j * h)) +
                    (1 - w) * k_u[i][j];
            }
        }

        flag = false;

        for (int i = 0; i <= n; i++) {
            for (int j = 0; j <= m; j++) {
                if (abs(u[i][j] - k_u[i][j]) < error) {
                    continue;
                } else {
                    flag = true;
                }
            }
        }
        k++;
    }

    for (int j = m; j >= 0; j--) {
        for (int i = 0; i <= n; i++) {
            cout << u[i][j] << "\t";
        }
        cout << "\n";
    }

    cout << "Number of iterations: " << k << "\n";

    return 0;
}