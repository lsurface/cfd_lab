#include <iostream>

using namespace std;

int main() {
    int n, m;

    cout << "Enter value for n: ";
    cin >> n;

    cout << "Enter value for m: ";
    cin >> m;

    // Will be used as (k+1)th iteration
    float u[n + 1][m + 1] = {0};

    // Will be used as kth iteration
    float k_u[n + 1][m + 1] = {0};

    // Setting vertical values
    for (int i = 0; i <= m; i++) {
        u[0][i] = 0;
        k_u[0][i] = 0;

        u[m][i] = 0;
        k_u[m][i] = 0;
    }

    // Setting horizontal values
    for (int i = 0; i <= n; i++) {
        u[i][0] = 100;
        k_u[i][0] = 100;

        u[i][n] = 0;
        k_u[i][n] = 0;
    }

    bool flag = true;
    float error = 0.005;
    int k = 1;

    while (flag) {
        // Updating k_u for (k+1)th iteration
        for (int i = 1; i < n; i++) {
            for (int j = 1; j < m; j++) {
                k_u[i][j] = u[i][j];
            }
        }
        for (int i = 1; i < n; i++) {
            for (int j = 1; j < m; j++) {
                u[i][j] = 0.25 * (u[i][j + 1] + u[i - 1][j] + k_u[i][j - 1] +
                                  k_u[i + 1][j]);
            }
        }

        flag = false;
        for (int i = 1; i < n; i++) {
            for (int j = 1; j < m; j++) {
                if (abs(u[i][j] - k_u[i][j]) < error) {
                    continue;
                } else {
                    flag = true;
                }
            }
        }
        k++;
    }

    for (int j = m; j >= 0; j--) {
        for (int i = 0; i <= n; i++) {
            cout << u[i][j] << "\t";
        }
        cout << "\n";
    }
    cout << "Number of iterations: " << k << "\n";

    return 0;
}