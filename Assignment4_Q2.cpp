#include <math.h>
#include <iostream>

using namespace std;

float initial_condition(float x) {

    return 10 + x * (1 - x);
}

int main() {

    float lower_x = 0;
    float upper_x = 1;
    float h = 0.2;

    float lower_t = 0;
    float upper_t = 1;
    float k = 0.2;

    int n = (int)((upper_x - lower_x) / h);
    int m = (int)((upper_t - lower_t) / k);

    float u[n + 1][m + 1];

    // Setting boundary conditions
    for (int i = 0; i <= m; i++) {
        u[0][i] = 0;
        u[n][i] = 0;
    }

    // Setting initial conditions
    for (int i = 1; i < n; i++) {
        u[i][0] = initial_condition(i * h);
    }

    float r = k / h;

    // Applying initial velocity condition on first time level
    for (int i = 1; i < n; i++) {
        u[i][1] = k * 0 + u[i][0];
    }

    // Applying FDM for remaining mesh points
    for (int j = 2; j <= m; j++) {
        for (int i = 1; i < n; i++) {
            u[i][j] = 2 * (1 - pow(r, 2)) * u[i][j - 1] +
                      pow(r, 2) * (u[i + 1][j - 1] + u[i - 1][j - 1]) -
                      u[i][j - 2];
        }
    }

    // Displaying values
    for (int j = m; j >= 0; j--) {
        for (int i = 0; i <= n; i++) {
            cout << u[i][j] << "\t";
        }
        cout << "\n";
    }

    return 0;
}
