#include <math.h>

#include <iostream>

using namespace std;

float alpha;

// Setting the initial value condition
float initial_value(float x) {
    return 0.5 * x * (8 - x);
}

float compute(float u1, float u2, float u3) {
    float u = alpha * u1 + (1 - 2 * alpha) * u2 + alpha * u3;

    return u;
}

int main() {
    float h, k;
    int c = 1, n, t;
    cout << "Enter value for h\n";
    cin >> h;

    cout << "Enter value for k\n";
    cin >> k;

    cout << "Enter number of time levels\n";
    cin >> t;

    n = 8 / h;

    float u[n + 1][t + 1];

    alpha = (pow(c, 2) * k) / pow(h, 2);

    // Setting initial value conditions
    for (int i = 0; i <= n; i++) {
        u[i][0] = initial_value(i * h);
    }

    // Setting boundary value conditions
    for (int i = 0; i <= t; i++) {
        u[0][i] = 0;
        u[n][i] = 0;
    }

    // Computing mesh values
    for (int j = 1; j <= t; j++) {
        for (int i = 1; i < n; i++) {
            u[i][j] = compute(u[i - 1][j - 1], u[i][j - 1], u[i + 1][j - 1]);
        }
    }

    for (int j = 0; j <= t; j++) {
        cout << "values at t=" << j << "\n";
        for (int i = 0; i <= n; i++) {
            cout << "u[" << i << "][" << j << "] = " << u[i][j] << "\n";
        }
        cout << "\n";
    }

    return 0;
}